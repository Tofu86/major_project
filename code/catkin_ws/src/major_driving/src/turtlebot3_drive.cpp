/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Authors: Taehun Lim (Darby) */



#include "major_driving/turtlebot3_drive.h"
#include <chrono>
#include <thread>
#include <iostream>
//the constructor of the drive
Turtlebot3Drive::Turtlebot3Drive()
  : nh_priv_("~")
{
  //Init gazebo ros turtlebot3 node
  ROS_INFO("TurtleBot3 Simulation Node Init");
  ROS_ASSERT(init());
}
//descructor
Turtlebot3Drive::~Turtlebot3Drive()
{
  updatecommandVelocity(0.0, 0.0);
  ros::shutdown();
}

/*******************************************************************************
* Init function
*******************************************************************************/
//init function to initialize the member variable
bool Turtlebot3Drive::init()
{
  // initialize ROS parameter
  std::string cmd_vel_topic_name = nh_.param<std::string>("cmd_vel_topic_name", "");

  // initialize variables
  escape_range_ = 30.0 * DEG2RAD;
  check_forward_dist_ = 0.25;
  check_30min_dist = 0.29; // forward distance / cos30
  check_leftmax_dist_ = 0.3;
  check_leftmin_dist_ = 0.25;
  current_button_state = 1;
  button_value = 0;
  tb3_pose_ = 0.0;
  prev_tb3_pose_ = 0.0;
  charge_flag = 0;
  input_data = "";
  previous_data = "";

  // initialize publishers
  cmd_vel_pub_   = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);

  // initialize subscribers
  laser_scan_sub_  = nh_.subscribe("scan", 10, &Turtlebot3Drive::laserScanMsgCallBack, this);
  odom_sub_ = nh_.subscribe("odom", 10, &Turtlebot3Drive::odomMsgCallBack, this);
  //sensor_state_sub_ = nh_.subscribe("button", 10, &Turtlebot3Drive::sensorStateMsgCallback, this);

  info_sub_ = nh_.subscribe("information_publisher", 10, &Turtlebot3Drive::informationCallBack, this);
  return true;
}
//the function to get the position value from turtlebot
void Turtlebot3Drive::odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg)
{
  double siny = 2.0 * (msg->pose.pose.orientation.w * msg->pose.pose.orientation.z + msg->pose.pose.orientation.x * msg->pose.pose.orientation.y);
	double cosy = 1.0 - 2.0 * (msg->pose.pose.orientation.y * msg->pose.pose.orientation.y + msg->pose.pose.orientation.z * msg->pose.pose.orientation.z);

	tb3_pose_ = atan2(siny, cosy);
}
//the function to get the lidar data from the turtlebot and store the selected angle data to scan_data
void Turtlebot3Drive::laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  //set the angle needed to be detected
  //uint16_t scan_angle[7] = {180, 210, 150,225,135,270,90};
  uint16_t scan_angle[7] = {0, 30, 330,45,315,90,270};
  for (int num = 0; num < 7; num++)
  {
    //check if the value is out of limit, which will return 0
    if (msg->ranges.at(scan_angle[num]) == 0)
    {
      scan_data_[num] = msg->range_max;
    }
    else
    {
      scan_data_[num] = msg->ranges.at(scan_angle[num]);
    }
  }
}

void Turtlebot3Drive::informationCallBack(const std_msgs::String::ConstPtr& msg)
{

  input_data = msg->data;
  if(input_data != previous_data && input_data == "End")
  {

    updatecommandVelocity(0,0);
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "\n\n---------------------------" << std::endl;
    std::cout << "The scan is finished." << std::endl;
    std::cout << "Press 1 to charing the robot." <<std::endl;
    std::cout << "Press 2 to rescan." <<std::endl;
    std::cout << "---------------------------\n" << std::endl;
    while(1)
    {
      std::cout << "Userinput: ";
      std::cin >> input_end;
      if(input_end == "1")
      {

        std::cout << "\nGoing to the charging station" << std::endl;
        charge_flag = 1;
        break;
      }
      else if(input_end == "2")
      {

        std::cout << "\nContinue scanning" <<std::endl;
        break;
      }
      else
      {
        std::cout << "Wrong input, please input 1 or 2." << std::endl;
      }
    }
  }
  else if(input_data != previous_data && input_data == "Dock" && charge_flag == 1)
  {
    updatecommandVelocity(0,0);

    std::cout << "\n\n---------------------------" << std::endl;
    std::cout << "Charging station arrived!"  << std::endl;
    std::cout << "System shut down in 5 seconds" << std::endl;
    std::cout << "---------------------------" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "System shutdown!!! " << std::endl;
    ros::shutdown();
  }
  previous_data = input_data;
  // //std::cout << msg->data << std::endl;
  // if (msg->data == "Dock") {
  //   ROS_INFO("<CHARGING.>");
  //   updatecommandVelocity(0.0, 0.0);
  //   ros::shutdown();
  //   //ROS_INFO("I heard: [%s]", msg->data.c_str());
  }

//update linear velocityand angular velocity to turtlebot
void Turtlebot3Drive::updatecommandVelocity(double linear, double angular)
{
  geometry_msgs::Twist cmd_vel;

  cmd_vel.linear.x  = linear;
  cmd_vel.angular.z = angular;

  cmd_vel_pub_.publish(cmd_vel);
}

// void Turtlebot3Drive::sensorStateMsgCallback(const turtlebot3_msgs::SensorState::ConstPtr &msg)
// {
//   button_value = msg->button;
//   if (msg->button == turtlebot3_msgs::SensorState::BUTTON0)
//     current_button_state = 1;
//   else if (msg->button == turtlebot3_msgs::SensorState::BUTTON1)
//     current_button_state = 2;
//   else
//     std::cout<< "Nothing PUSHED"<<std::endl;
//   ROS_INFO("%d", msg->button);
// }


/*******************************************************************************
* Control Loop function
*******************************************************************************/
bool Turtlebot3Drive::controlLoop()
{
  //initialize state number to be LEFT_priority
  static uint8_t turtlebot3_state_num = 6;
  // if(scan_data_[LEFT_30]<0.2)
  // {
  //   std::cout <<"left30" << std::endl;
  // }
  // else if(scan_data_[LEFT_45]<0.2)
  // {
  //   std::cout <<"left45" << std::endl;
  // }
  // else if(scan_data_[CENTER]<0.2)
  // {
  //   std::cout <<"CENTER" << std::endl;
  // }
  switch(turtlebot3_state_num)
  {
    //decide to turn or drive forward according to distance value
    case LEFT_PRIORITY:
      //update the pose data
      prev_tb3_pose_ = tb3_pose_;
      //check and avoid the error
      if (fabs(prev_tb3_pose_ - tb3_pose_) >= escape_range_)
      {break;}
      else
      {
        //check the the front and left 30 degree distance to see whether there are
        //obstacles in front of the robot
        if(scan_data_[CENTER] > check_forward_dist_ && scan_data_[LEFT_30] > check_30min_dist && scan_data_[RIGHT_30] > check_30min_dist)
        {
          //check the left45 distance to determine whether we need to modify the direction
          //the left distance is larger than high limit so turn left while driving forward
          if (scan_data_[LEFT_45] > check_leftmax_dist_)
          {

            turtlebot3_state_num = TB3_LEFT_TURN;
          }

          //the left distance is lower than low limit so turn right while driving forward
          else if(scan_data_[LEFT_45] < check_leftmin_dist_)
          {
            turtlebot3_state_num = TB3_RIGHT_TURN;
          }
          //the robot is in the range of position so go straight
          else
          {
            turtlebot3_state_num = TB3_DRIVE_FORWARD;
          }
        }
        //the front distance is lower than limit so need to go back
        else
        {
          // if (scan_data_[LEFT_45] > check_leftmax_dist_)
          // {
          //   turtlebot3_state_num = TB3_DRIVE_RIGHT_BACKWARD;
          // }
          // else
          // {
          //   turtlebot3_state_num = TB3_DRIVE_BACKWARD;
          // }
          updatecommandVelocity(0,-0.7 * ANGULAR_VELOCITY);

        }
      }

        break;

    case TB3_DRIVE_RIGHT_BACKWARD:
      updatecommandVelocity(-1 * LINEAR_VELOCITY,-1.5 * ANGULAR_VELOCITY);
      turtlebot3_state_num = LEFT_PRIORITY;
    break;

    case TB3_DRIVE_BACKWARD:
      updatecommandVelocity(0.0, 1 * ANGULAR_VELOCITY);
      turtlebot3_state_num = LEFT_PRIORITY;

    break;

    case TB3_DRIVE_FORWARD:
      updatecommandVelocity(LINEAR_VELOCITY, 0.0);
      turtlebot3_state_num = LEFT_PRIORITY;

      break;

    case TB3_RIGHT_TURN:
      updatecommandVelocity(LINEAR_VELOCITY,-1 * ANGULAR_VELOCITY);
      turtlebot3_state_num = LEFT_PRIORITY;

      break;

    case TB3_LEFT_TURN:

      updatecommandVelocity(LINEAR_VELOCITY,1 * ANGULAR_VELOCITY);
      turtlebot3_state_num = LEFT_PRIORITY;
      break;

    default:
      turtlebot3_state_num = LEFT_PRIORITY;
      break;
  }

  return true;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "turtlebot3_drive");
  Turtlebot3Drive turtlebot3_drive;
  //set the refresh rate
  ros::Rate loop_rate(125);


  //wait 2 seconds to start
  std::cout << "\n\n\nThe tobot will start scanning  in 15 secends." << std::endl;
  std::cout << "Please put the robot to start point.\n" <<std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(15));
  std::cout << "Welcome to Antelope Grocery"<<std::endl;


  while (ros::ok())
  {

    turtlebot3_drive.controlLoop();

    ros::spinOnce();
    loop_rate.sleep();
  }



  return 0;
}
