# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/catkin_ws/install/include".split(';') if "/home/pi/catkin_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;nav_msgs;roscpp;sensor_msgs;std_msgs;tf;turtlebot3_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lmajor_driving".split(';') if "-lmajor_driving" != "" else []
PROJECT_NAME = "major_driving"
PROJECT_SPACE_DIR = "/home/pi/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
