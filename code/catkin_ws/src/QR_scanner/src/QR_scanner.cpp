// include lib
#include <iostream>
#include <string>
#include <vector>

#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/String.h"
#include <sstream>

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <zbar.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
//data processing include library
#include <iostream>
#include <vector>
#include <string.h>
#include <iterator>
#include <stdio.h>
#include <fstream>
//QR scan namespace define
using namespace cv ;
using namespace zbar ;
using namespace std ;
//global variable
std::string input_data ="";
std::string previous_input = "";
std::ofstream OutFile("/home/pi/catkin_ws/example.txt");
int scanNum = 1;
int Apple_Num;
int Banana_Num;
int Toast_Num;
int Potato_Num;
int Magazine_Num;
//
//QR scan class implementation
struct QR_Code
{
    string data ;
    vector<Point> location ;
} ;

class Scanner_cam
{
    public:
    //create a handle to node
    ros::NodeHandle scanner_nh ;
    ros::NodeHandle _n;
    // initialize publishers
    ros::Publisher info_pub_  = _n.advertise<std_msgs::String>("information_publisher", 10);


    //find the qr code and decode
    void scanimg(Mat &img, vector<QR_Code> &qritem_list)
    {
        this->scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1) ;
        //convert image to grey style
        Mat Grey_img ;
        cvtColor(img, Grey_img,COLOR_BGR2GRAY) ;
        //trans image data to zbar image
        Image image(img.cols, img.rows, "Y800", (uchar *)Grey_img.data, img.cols*img.rows) ;


        //scan the image and decode data
        int qr_scandata = scanner.scan(image) ;
        //save the data to object
        for (Image::SymbolIterator Symbol_it = image.symbol_begin() ;
                Symbol_it!=image.symbol_end() ; ++Symbol_it )
        {
            //get the qr data
            string item_data = Symbol_it->get_data() ;
            //create a new QR code object
            QR_Code qr_result ;
            //store the data to "data"
            qr_result.data = item_data ;
            for (int pos = 0 ; pos < Symbol_it->get_location_size() ; pos++)
            {
                qr_result.location.push_back(
                  //exact location information
                    Point(Symbol_it->get_location_x(pos),Symbol_it->get_location_y(pos))) ;
            }
            //push back the result to result vector
            qritem_list.push_back(qr_result) ;
            input_data = item_data;

            std_msgs::String msg;
            std::stringstream ss;
            ss << input_data;
            msg.data = ss.str();

//            ROS_INFO("%s",item_data.c_str()) ;
            info_pub_.publish(msg);

        }
    }

    void display(Mat &img, vector<QR_Code> &qr_codes)
    {
        for (int counter = 0 ; counter <qr_codes.size() ; counter++)
        {
            vector<Point> points = qr_codes[counter].location ;

            for (int pt = 0; pt<points.size(); pt++)
            {
                line(img, points[pt],points[(pt+1)%points.size()],
                        Scalar(255,0,0), 3);
            }
        }
        imshow("QR_scanned", img) ;
        waitKey(3);
    }

    private:
    zbar::ImageScanner scanner ;
    // ROS Topic Publishers
    //ros::Publisher info_pub_;
} ;
//cv bridge to convert ras image to zbar image
class img_Convertor
{
    public:
    //subsribe to an image topic /raspicam_node/image
    img_Convertor():it_(img_convertor_nh)
    {
        IT_sub = it_.subscribe("/raspicam_node/image",1,
            &img_Convertor::imgCallBack, this) ;
    }

    ~img_Convertor()
    {
        //destroy a display window when shui down
        destroyAllWindows() ;
    }

    void imgCallBack(const sensor_msgs::ImageConstPtr &img)
    {
        //use cv_bridge to convert ROS message to opencv image
        cv_bridge::CvImagePtr cv_ptr ;
        try
        {
            //make copy to picture
            cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        cv::Mat im = cv_ptr->image;
        std::vector<QR_Code> qr_codes;
        scanner1.scanimg(im, qr_codes);
        scanner1.display(im, qr_codes);
    }

    private:
    ros::NodeHandle img_convertor_nh ;
    image_transport::ImageTransport it_ ;
    image_transport::Subscriber IT_sub ;
    Scanner_cam scanner1 ;
} ;


//data processing clas definition
// void Add2Shelf(int choice, int ShelfN);
//-----------------------------------------------------------------------
//class defination
class Shelf {
public:
	Shelf(int n) {
    _AppleNum = 0;
    _BananaNum = 0;
    _ToastNum = 0;
    _PotatoNum = 0;
    _MagazineNum = 0;
    _shelfN = n; }
	void addApple() { _AppleNum++; }
	void addBanana() { _BananaNum++; }
  void addToast() { _ToastNum++; }
  void addPotato() { _PotatoNum++; }
  void addMagazine() { _MagazineNum++; }
	void Report();
	int getN() { return _shelfN; }
	static Shelf* addSh(int ShelfN);
private:
	int _AppleNum;
	int _BananaNum;
  int _ToastNum;
  int _PotatoNum;
  int _MagazineNum;
	int _shelfN;
};

class Item {
public:
	virtual void message() = 0;
	static Item* addItem(int choice, int ID, int Shelf);
	virtual int getID() = 0;
};

class Apple : public Item {
public:
	void message();
	Apple(int ID, int Shelf) { _ID = ID; _Shelf = Shelf; }
	int getID() { return _ID; }
private:
	int _ID;
	int _Shelf;
};

class Banana : public Item {
public:
	void message();
	Banana(int ID, int Shelf) { _ID = ID; _Shelf = Shelf; }
	int getID() { return _ID; }
private:
	int _ID;
	int _Shelf;
};

class Toast : public Item {
public:
	void message();
	Toast(int ID, int Shelf) { _ID = ID; _Shelf = Shelf; }
	int getID() { return _ID; }
private:
	int _ID;
	int _Shelf;
};

class Potato : public Item {
public:
	void message();
	Potato(int ID, int Shelf) { _ID = ID; _Shelf = Shelf; }
	int getID() { return _ID; }
private:
	int _ID;
	int _Shelf;
};

class Magazine : public Item {
public:
	void message();
	Magazine(int ID, int Shelf) { _ID = ID; _Shelf = Shelf; }
	int getID() { return _ID; }
private:
	int _ID;
	int _Shelf;
};

//the class to process the qr data
class Data_factory
{
public:
	//Constructor
	Data_factory() {};
	~Data_factory() {};
	int Processing_qrdata(std::string input_data);
	void Report();

private:
	std::vector<Item*> AppleList;
	std::vector<Item*> BananaList;
  std::vector<Item*> ToastList;
  std::vector<Item*> PotatoList;
  std::vector<Item*> MagazineList;
	std::vector<Item*>::iterator Iter;
	std::vector<Shelf*> ShelfList;
	std::vector<Shelf*>::iterator IterShelf;

	int choice;
	char info[100];
	int ID;
	int Shelfs;
	char* token;
	int NewItem;
	int NewShelf;
};


/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char** argv)
{
    //initialize the name of the node
    ros::init(argc,argv, "Qr_scanner") ;
    //create a image convertor class
    img_Convertor img_convertor1;

    ros::Rate loop_rate(50);
    std::ofstream OutFile("example.txt");

  	Data_factory factory1;

    while (1)
    {
      /**
       * ros::spin() will enter a loop, pumping callbacks.  With this version, all
       * callbacks will be called from within this thread (the main one).  ros::spin()
       * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
       */

        ros::spinOnce();
        loop_rate.sleep();
        //cout << "|" <<input_data<<"|"<<endl;
        if (input_data == previous_input)
        {
          continue;
        }


        if (factory1.Processing_qrdata(input_data) == 0)
    		{
    			break;
    		}

    }

    return 0 ;
}
/*******************************************************************************
* class implementation
*******************************************************************************/

//class Data_factory implementation

int Data_factory::Processing_qrdata(std::string input_data)
{

	strcpy(info, input_data.c_str());
  previous_input = input_data;

	token = strtok(info, " ");
	std::string name(token);
	NewItem = 1;
	NewShelf = 1;

	if (name == "End" && input_data != "") {
		//break the loop

		this->Report();

		if (scanNum ==1) {  // first round of scan
			scanNum++;
			Apple_Num = AppleList.size();
			Banana_Num = BananaList.size();
      Toast_Num = ToastList.size();
      Potato_Num = PotatoList.size();
      Magazine_Num = MagazineList.size();
		} else {		// second round of scan
			if (AppleList.size() != Apple_Num) {
				std::cout << "[ Changes ] Number of apples changed from " << Apple_Num << " to " << AppleList.size() << "." << std::endl;
				Apple_Num = AppleList.size();
			}
			if (BananaList.size() != Banana_Num) {
				std::cout << "[ Changes ] Number of bananas changed from " << Banana_Num << " to " << BananaList.size() << "." << std::endl;
				Banana_Num = BananaList.size();
			}
      if (ToastList.size() != Toast_Num) {
				std::cout << "[ Changes ] Number of toast changed from " << Toast_Num << " to " << ToastList.size() << "." << std::endl;
				Toast_Num = ToastList.size();
			}
      if (PotatoList.size() != Potato_Num) {
				std::cout << "[ Changes ] Number of potatoes changed from " << Potato_Num << " to " << PotatoList.size() << "." << std::endl;
				Potato_Num = PotatoList.size();
			}
      if (MagazineList.size() != Magazine_Num) {
				std::cout << "[ Changes ] Number of magazines changed from " << Magazine_Num << " to " << MagazineList.size() << "." << std::endl;
				Magazine_Num = MagazineList.size();
			}

		}
		AppleList.clear();
		BananaList.clear();
    ToastList.clear();
    PotatoList.clear();
    BananaList.clear();
		MagazineList.clear();
		name.clear();
		cout << name <<endl;

		//return 0;
	}
	if (name == "Dock" && scanNum == 2 && input_data != ""){ // if two scans finished and the robot reaches the charging dock
		
		AppleList.clear();
		BananaList.clear();
		ShelfList.clear();
		name.clear();
		}
	if (name == "Apple") {
		choice = 1;
		token = strtok(NULL, " ");
		ID = atoi(token);
		for (Iter = AppleList.begin(); Iter != AppleList.end(); Iter++) {
			if (ID == (*Iter)->getID()) {
				NewItem = 0;
				break;
			}
		}
		if (NewItem == 1) {
			token = strtok(NULL, " ");
			Shelfs = atoi(token);
			AppleList.push_back(Item::addItem(choice, ID, Shelfs));
			for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
				if (Shelfs == (*IterShelf)->getN()) {
					NewShelf = 0;
					(*IterShelf)->addApple();
					break;
				}
			}
			if (NewShelf == 1) {
				ShelfList.push_back(Shelf::addSh(Shelfs));
				ShelfList.back()->addApple();
			}
		}
	}
	else if (name == "Banana") {
		choice = 2;
		token = strtok(NULL, " ");
		ID = atoi(token);
		for (Iter = BananaList.begin(); Iter != BananaList.end(); Iter++) {
			if (ID == (*Iter)->getID()) {
				NewItem = 0;
				break;
			}
		}
		if (NewItem == 1) {
			token = strtok(NULL, " ");
			Shelfs = atoi(token);
			BananaList.push_back(Item::addItem(choice, ID, Shelfs));
			for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
				if (Shelfs == (*IterShelf)->getN()) {
					NewShelf = 0;
					(*IterShelf)->addBanana();
					break;
				}
			}
			if (NewShelf == 1) {
				ShelfList.push_back(Shelf::addSh(Shelfs));
				ShelfList.back()->addBanana();
			}
		}
	}
  if (name == "Toast") {
		choice = 3;
		token = strtok(NULL, " ");
		ID = atoi(token);
		for (Iter = ToastList.begin(); Iter != ToastList.end(); Iter++) {
			if (ID == (*Iter)->getID()) {
				NewItem = 0;
				break;
			}
		}
		if (NewItem == 1) {
			token = strtok(NULL, " ");
			Shelfs = atoi(token);
			ToastList.push_back(Item::addItem(choice, ID, Shelfs));
			for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
				if (Shelfs == (*IterShelf)->getN()) {
					NewShelf = 0;
					(*IterShelf)->addToast();
					break;
				}
			}
			if (NewShelf == 1) {
				ShelfList.push_back(Shelf::addSh(Shelfs));
				ShelfList.back()->addToast();
			}
		}
	}
  if (name == "Potato") {
		choice = 4;
		token = strtok(NULL, " ");
		ID = atoi(token);
		for (Iter = PotatoList.begin(); Iter != PotatoList.end(); Iter++) {
			if (ID == (*Iter)->getID()) {
				NewItem = 0;
				break;
			}
		}
		if (NewItem == 1) {
			token = strtok(NULL, " ");
			Shelfs = atoi(token);
			PotatoList.push_back(Item::addItem(choice, ID, Shelfs));
			for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
				if (Shelfs == (*IterShelf)->getN()) {
					NewShelf = 0;
					(*IterShelf)->addPotato();
					break;
				}
			}
			if (NewShelf == 1) {
				ShelfList.push_back(Shelf::addSh(Shelfs));
				ShelfList.back()->addPotato();
			}
		}
	}
  if (name == "Magazine") {
		choice = 5;
		token = strtok(NULL, " ");
		ID = atoi(token);
		for (Iter = MagazineList.begin(); Iter != MagazineList.end(); Iter++) {
			if (ID == (*Iter)->getID()) {
				NewItem = 0;
				break;
			}
		}
		if (NewItem == 1) {
			token = strtok(NULL, " ");
			Shelfs = atoi(token);
			MagazineList.push_back(Item::addItem(choice, ID, Shelfs));
			for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
				if (Shelfs == (*IterShelf)->getN()) {
					NewShelf = 0;
					(*IterShelf)->addMagazine();
					break;
				}
			}
			if (NewShelf == 1) {
				ShelfList.push_back(Shelf::addSh(Shelfs));
				ShelfList.back()->addMagazine();
			}
		}
	}

  input_data.clear();
	return 1;
}

void Data_factory::Report()
{	//std::ofstream OutFile("example.txt");
	std::cout << " \nNew Scan\n " << std::endl;
	if(OutFile.is_open()){
		OutFile << " \nNew Scan\n " << std::endl;
	}
	for (int i = 0; i < AppleList.size(); i++) {
		AppleList[i]->message();
	}
	
	for (int i = 0; i < BananaList.size(); i++) {
		BananaList[i]->message();
	}

  for (int i = 0; i < ToastList.size(); i++) {
		ToastList[i]->message();
	}
	

  for (int i = 0; i < PotatoList.size(); i++) {
		PotatoList[i]->message();
	}
	

  for (int i = 0; i < MagazineList.size(); i++) {
		MagazineList[i]->message();
	}
	
	std::cout << "[ Summary ] " << AppleList.size() << " apples added." << std::endl;
  if(OutFile.is_open()){
		OutFile << "[ Summary ] " << AppleList.size() << " apples added." << std::endl;
	}

	std::cout << "[ Summary ] " << BananaList.size() << " bananas added." << std::endl;
  if(OutFile.is_open()){
		OutFile << "[ Summary ] " << BananaList.size() << " bananas added." << std::endl;
	}
	
	std::cout << "[ Summary ] " << ToastList.size() << " Toast added." << std::endl;
  if(OutFile.is_open()){
		OutFile << "[ Summary ] " << ToastList.size() << " Toast added." << std::endl;
	}
	
	std::cout << "[ Summary ] " << PotatoList.size() << " Potato added." << std::endl;
  if(OutFile.is_open()){
		OutFile << "[ Summary ] " << PotatoList.size() << " Potato added." << std::endl;
	}
	
	std::cout << "[ Summary ] " << MagazineList.size() << " Magazine added." << std::endl;
  if(OutFile.is_open()){
		OutFile << "[ Summary ] " << MagazineList.size() << " Magazine added." << std::endl;
	}

  std::cout << "\n" << std::endl;
  if(OutFile.is_open()){
		OutFile << "\n" << std::endl;
	}

	for (IterShelf = ShelfList.begin(); IterShelf != ShelfList.end(); IterShelf++) {
		(*IterShelf)->Report();
	}
  std::cout << "\n" << std::endl;
  if(OutFile.is_open()){
		OutFile << "\n" << std::endl;
	}
//	OutFile.close();
}

//-----------------------------------------------------------------------
// class implementation
void Apple::message() {
	//std::cout << "[ New ] Apple:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	if(OutFile.is_open()){
		OutFile << "[ New ] Apple:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	}
}

void Banana::message() {
	//std::cout << "[ New ] Banana:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	if(OutFile.is_open()){
		OutFile << "[ New ] Banana:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	}
}

void Toast::message() {
	//std::cout << "[ New ] Toast:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	if(OutFile.is_open()){
		OutFile << "[ New ] Toast:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	}
}

void Potato::message() {
	//std::cout << "[ New ] Potato:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	if(OutFile.is_open()){
		OutFile << "[ New ] Potato:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	}
}

void Magazine::message() {
	//std::cout << "[ New ] Magazine:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	if(OutFile.is_open()){
		OutFile << "[ New ] Magazine:" << _ID << " on shelf " << _Shelf << " is scanned." << std::endl;
	}
}

void Shelf::Report() {
	std::cout << "[ Summary ] There are " << _AppleNum << " apples on shelf " << _shelfN << std::endl;
	std::cout << "[ Summary ] There are " << _BananaNum << " bananas on shelf " << _shelfN << std::endl;
  std::cout << "[ Summary ] There are " << _ToastNum << " toast on shelf " << _shelfN << std::endl;
  std::cout << "[ Summary ] There are " << _PotatoNum << " potatoes on shelf " << _shelfN << std::endl;
  std::cout << "[ Summary ] There are " << _MagazineNum << " magazines on shelf " << _shelfN << std::endl;

	if(OutFile.is_open()){
		OutFile << "[ Summary ] There are " << _AppleNum << " apples on shelf " << _shelfN << std::endl;
		OutFile << "[ Summary ] There are " << _BananaNum << " bananas on shelf " << _shelfN << std::endl;
    OutFile << "[ Summary ] There are " << _ToastNum << " toast on shelf " << _shelfN << std::endl;
    OutFile << "[ Summary ] There are " << _PotatoNum << " potatoes on shelf " << _shelfN << std::endl;
    OutFile << "[ Summary ] There are " << _MagazineNum << " magazines on shelf " << _shelfN << std::endl;
	}
}

// Factory Method
Item* Item::addItem(int choice, int ID, int Shelf)
{
	switch (choice) {
	case 1:
		return new Apple(ID, Shelf);
		break;
	case 2:
		return new Banana(ID, Shelf);
		break;
  case 3:
		return new Toast(ID, Shelf);
		break;
  case 4:
		return new Potato(ID, Shelf);
		break;
  case 5:
		return new Magazine(ID, Shelf);
		break;
	}
}

Shelf* Shelf::addSh(int ShelfN)
{
	return new Shelf(ShelfN);
}
