# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/catkin_ws/install/include".split(';') if "/home/pi/catkin_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "nodelet;cv_bridge;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbarcode_reader_nodelet".split(';') if "-lbarcode_reader_nodelet" != "" else []
PROJECT_NAME = "zbar_ros"
PROJECT_SPACE_DIR = "/home/pi/catkin_ws/install"
PROJECT_VERSION = "0.1.0"
