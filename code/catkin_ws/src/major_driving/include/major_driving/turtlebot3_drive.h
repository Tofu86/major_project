/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Authors: Taehun Lim (Darby) */

#ifndef TURTLEBOT3_DRIVE_H_
#define TURTLEBOT3_DRIVE_H_

#include <ros/ros.h>
#include "std_msgs/String.h"

#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

#include <turtlebot3_msgs/SensorState.h>
#define DEG2RAD (M_PI / 180.0)
#define RAD2DEG (180.0 / M_PI)
//define the constant calue for different lidar angle data
#define CENTER 0
#define LEFT_30 1
#define RIGHT_30 2
#define LEFT_45 3
#define RIGHT_45 4
#define LEFT_90 5
#define RIGHT_90 6


//define the standard lenear velocity and angular velocity
#define LINEAR_VELOCITY  0.1
#define ANGULAR_VELOCITY 0.6

//define contant for state number
#define TB3_DRIVE_FORWARD 1
#define TB3_RIGHT_TURN    2
#define TB3_LEFT_TURN     3
#define TB3_DRIVE_RIGHT_BACKWARD 4
#define TB3_DRIVE_BACKWARD 5
#define LEFT_PRIORITY     6


class Turtlebot3Drive
{
 public:
  Turtlebot3Drive();
  ~Turtlebot3Drive();
  //constructors for Turtlebotdrive. Initialize the values and setup publisher and susbriber
  bool init();
  //detect the left45, left30 ,front and right30 distance to follow the left wall to go across the puzzle
  bool controlLoop();

  double current_button_state;
  double button_value;
  std::string input_data;
  std::string previous_data;
  std::string input_end;
  int charge_flag;
 private:
  // ROS NodeHandle
  ros::NodeHandle nh_;
  ros::NodeHandle nh_priv_;

  // ROS Parameters

  // ROS Time

  // ROS Topic Publishers
  ros::Publisher cmd_vel_pub_;

  // ROS Topic Subscribers
  ros::Subscriber laser_scan_sub_;
  ros::Subscriber odom_sub_;
  ros::Subscriber sensor_state_sub_;

  ros::Subscriber info_sub_;


  // Variables
  double escape_range_;
  //the max distance value for forward distance
  double check_forward_dist_;
  //the max and min distance value for left 45 distance
  double check_leftmax_dist_;
  double check_leftmin_dist_;
  //the min distance value for left and right 30 distance
  double check_30min_dist;
  double scan_data_[7] = {0.0, 0.0, 0.0, 0.0, 0.0,0.0,0.0};
  //position data of the turtlebot
  double tb3_pose_;
  double prev_tb3_pose_;

  // Function prototypes
  //update the linear velocity and anglar velocity to the turtlebot
  void updatecommandVelocity(double linear, double angular);
  //receive the lidar data
  void laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg);
  //receive the position data
  void odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg);
  // void sensorStateMsgCallback(const turtlebot3_msgs::SensorState::ConstPtr &msg);
  void informationCallBack(const std_msgs::String::ConstPtr& msg);
};
#endif // TURTLEBOT3_DRIVE_H_
